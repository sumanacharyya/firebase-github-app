import React, { useEffect, useState } from "react";
import { ListGroup, ListGroupItem } from "reactstrap";
import axios from "axios";

const Repos = ({ repos_url }) => {
  const [repos, setRepos] = useState([]);

  const fetchRepo = async () => {
    const { data } = await axios.get(repos_url);
    setRepos(data);
  };

  useEffect(() => {
    fetchRepo();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [repos_url]);

  return (
    <ListGroup>
      {repos.map((eachRepo) => (
        <ListGroupItem key={eachRepo.id}>
          <div className="text-primary">{eachRepo?.name}</div>
          <div className="text-secondary">{eachRepo?.language}</div>
          <div className="text-info">{eachRepo?.description}</div>
        </ListGroupItem>
      ))}
    </ListGroup>
  );
};

export default Repos;
