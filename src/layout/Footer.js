import React from "react";
import { Container } from "reactstrap";

const currentYear = new Date().getFullYear();

const Footer = () => {
  return (
    <Container
      fluid
      tag={"footer"}
      className="text-center bg-info text-white text-uppercase fixed-bottom p-3"
    >
      SUMAN ACHARYYA - All Rights Reserved &#169; - {currentYear}
    </Container>
  );
};

export default Footer;
