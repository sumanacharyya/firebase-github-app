import React, { useContext, useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavLink,
  NavItem,
  NavbarText,
} from "reactstrap";

import { Link } from "react-router-dom";

import { UserContext } from "../context/UserContext";

const Header = () => {
  const context = useContext(UserContext);
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => {
    setIsOpen(!isOpen);
  };

  return (
    <Navbar color="info" light expand="md">
      <NavbarBrand>
        <Link to={"/"} className="text-white">
          SUMAN-GITFIRE
        </Link>
      </NavbarBrand>
      <NavbarText className="text-white">
        {context.user?.email || ""}
      </NavbarText>
      <NavbarToggler onClick={toggle} />
      <Collapse navbar isOpen={isOpen}>
        <Nav navbar className="ml-auto">
          {" "}
          {/* Video no - 7 */}
          {context.user ? (
            <NavItem>
              {/* <NavLink tag={Link} to="/" className="text-white"> */}
              <NavLink
                onClick={() => context.setUser(null)}
                className="text-white"
              >
                LogOut
              </NavLink>
            </NavItem>
          ) : (
            <>
              <NavItem>
                <NavLink tag={Link} to="/signup" className="text-black">
                  SignUp
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/signin" className="text-white">
                  SignIn
                </NavLink>
              </NavItem>
            </>
          )}
        </Nav>
      </Collapse>
    </Navbar>
  );
};

export default Header;
