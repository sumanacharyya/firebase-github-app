import axios from "axios";
import React, { useContext, useState } from "react";

import {
  Button,
  Col,
  Container,
  Input,
  InputGroup,
  // InputGroupAddon
  InputGroupText,
  Row,
} from "reactstrap";

import { Navigate } from "react-router-dom";
import { toast } from "react-toastify";
import Repos from "../components/Repos";
import UserCard from "../components/UserCard";
import { UserContext } from "../context/UserContext";

const Home = () => {
  const context = useContext(UserContext);
  const [query, setQuery] = useState("");
  const [user, setUser] = useState(null);

  const fetchDetails = async () => {
    try {
      const { data } = await axios.get(`https://api.github.com/users/${query}`);
      console.log(data);
      setUser(data);
    } catch (error) {
      toast("Not able to locate the user", { type: "error" });
    }
  };

  // Put any page behind Login
  if (!context.user?.uid) {
    return <Navigate to="/signin" />;
  } else {
    return (
      <Container>
        <Row className=" mt-3">
          <Col md="5">
            <InputGroup>
              <Input
                type="text"
                value={query}
                placeholder="Please provide the username"
                onChange={(e) => setQuery(e.target.value)}
              />
              {/* <InputGroupAddon addonType="append"> */}
              <InputGroupText addonType="append">
                <Button onClick={fetchDetails} color="primary">
                  Fetch User
                </Button>
              </InputGroupText>
              {/* </InputGroupAddon> */}
            </InputGroup>
            {user ? <UserCard user={user} /> : null}
          </Col>
          <Col md="7">
            {user ? <Repos repos_url={user?.repos_url} /> : null}
          </Col>
        </Row>
      </Container>
    );
  }
};

export default Home;
